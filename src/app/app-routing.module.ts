import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'creer-use', redirectTo: 'creer-user.page', pathMatch: 'creer-user.page' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'creer-user', loadChildren: './creer-user/creer-user.module#CreerUserPageModule' },
  { path: 'detail-user', loadChildren: './detail-user/detail-user.module#DetailUserPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
